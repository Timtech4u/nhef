from rest_framework import routers
from .views import *

app_name = "api"

router = routers.SimpleRouter()
router.register(r"scholar", ScholarView)
router.register(r"event", EventView)
router.register(r"resource", ResourceView)
router.register(r"testimonial", TestimonialView)
router.register(r"opportunity", OpportunityView)
router.register(r"university", UniversityView)
router.register(r"news", NewsView)

urlpatterns = router.urls