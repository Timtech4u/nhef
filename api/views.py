from .models import *
from .serializers import *
from rest_framework.viewsets import ModelViewSet

class ScholarView(ModelViewSet):
    queryset = Scholar.objects.all()
    serializer_class = ScholarSerializer

class EventView(ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class ResourceView(ModelViewSet):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer

class TestimonialView(ModelViewSet):
    queryset = Testimonial.objects.all()
    serializer_class = TestimonialSerializer

class OpportunityView(ModelViewSet):
    queryset = Opportunity.objects.all()
    serializer_class = OpportunitySerializer

class UniversityView(ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer

class NewsView(ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
