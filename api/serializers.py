from .models import *
from rest_framework.serializers import ModelSerializer

class ScholarSerializer(ModelSerializer):
    class Meta:
        model = Scholar
        fields = "__all__"

class EventSerializer(ModelSerializer):
    class Meta:
        model = Event
        fields = "__all__"

class ResourceSerializer(ModelSerializer):
    class Meta:
        model = Resource
        fields = "__all__"

class TestimonialSerializer(ModelSerializer):
    class Meta:
        model = Testimonial
        fields = "__all__"

class OpportunitySerializer(ModelSerializer):
    class Meta:
        model = Opportunity
        fields = "__all__"

class UniversitySerializer(ModelSerializer):
    class Meta:
        model = University
        fields = "__all__"

class NewsSerializer(ModelSerializer):
    class Meta:
        model = News
        fields = "__all__"

