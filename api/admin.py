from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import *

@admin.register(Event)
class EventAdmin(ImportExportModelAdmin):
    pass

@admin.register(Scholar)
class ScholarAdmin(ImportExportModelAdmin):
    pass

@admin.register(Resource)
class ResourceAdmin(ImportExportModelAdmin):
    pass

@admin.register(Testimonial)
class TestimonialAdmin(ImportExportModelAdmin):
    pass

@admin.register(Opportunity)
class OpportunityAdmin(ImportExportModelAdmin):
    pass

@admin.register(University)
class UniversityAdmin(ImportExportModelAdmin):
    pass

@admin.register(News)
class NewsAdmin(ImportExportModelAdmin):
    pass
