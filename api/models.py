from django.db import models

class Scholar(models.Model):
    image = models.ImageField(blank=True , null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    school = models.CharField(max_length=50, blank=True, null=True)
    email = models.EmailField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    scholar_year = models.CharField(max_length=4, blank=True, null=True)

    def __str__(self):
        return self.name

class Event(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    location = models.CharField(max_length=50, blank=True, null=True)
    datetime = models.DateTimeField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.title

class Resource(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    url = models.URLField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.title

class Testimonial(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    date_posted = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name

class Opportunity(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    opportunity_type = models.CharField(max_length=50, blank=True, null=True)
    company = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    date_posted = models.DateField(blank=True, null=True)

    class Meta:
        verbose_name = "Opportunity"
        verbose_name_plural = "Opportunities"

    def __str__(self):
        return self.title

class University(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)


    class Meta:
        verbose_name = "University"
        verbose_name_plural = "Universities"

    def __str__(self):
        return self.name

class News(models.Model):
    title = models.CharField(max_length=50, blank=True, null=True)
    detail = models.TextField(blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True)
    author = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        verbose_name = "News"
        verbose_name_plural = "News"

    def __str__(self):
        return self.title